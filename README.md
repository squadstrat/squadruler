# SquadRuler

SquadRuler is a small map utility for the game Squad.

In essence, it's a screen measuring tool for relative size.
When used in Squad it allows accurate inference of distances between arbitrary points via the minimap and its grid or scale in bottom right.

### How To Use

Running the executable will create a full-screen, borderless, semi-transparent window on the primary monitor.
Holding down RMB or MMB, dragging the required distance and releasing will set the reference screen size. Then, dragging with LMB, will display a distance measurement.

It's currently intended to be used with an autohotkey script to activate/minimize the window to overlap it with Squad. 
This is simpler than messing around with windows api and providing configuration (might do later) and allows a bit 
more customization from the user without having to write and build rust code. A default script is included.

Other options would be Alt-Tab'ing into the window and back or using a hotkey set via windows UI and then closing via Alt-F4 when done - or any combination of these.

Known Issues:
Appears to sometimes cause ue4 crashes due to directx issues (probably clashing with the context from this program) - will fix if this ever leaves meme stage.
